import changeMenuPosition from './js/menu-position.js';
import changeMenuActiveItem from './js/menu-active-item.js'
import scrollMenu from './js/menu-scroll.js'
import showMenu from './js/show-menu-mobile.js'
import showScrollMobile from './js/menu-scroll-mobile.js'

$(document).ready(function() {

    $("#main-slider").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });

    $("#projects-slider").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        items : 4,
        itemsDesktop : [1400,3],
        itemsDesktopSmall : [994,2],
        itemsTablet: [668,1],
        itemsMobile : 1,
        navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });
    $("#team-slider").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        items : 4,
        itemsDesktop : [1400,3],
        itemsDesktopSmall : [994,2],
        itemsTablet: [668,1],
        itemsMobile : 1,
        navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });
    $("#news-slider").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        items : 2,
        itemsDesktop : [1400,1],
        itemsDesktopSmall : [1280,1],
        itemsTablet: [668,1],
        itemsMobile : 1,
        navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });
    $("#clients-slider").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        items : 8,
        itemsDesktop : [1400,7],
        itemsDesktopSmall : [1280,5],
        itemsTablet: [744,2],
        itemsMobile : 2,
        navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });


    changeMenuPosition();
    if (document.documentElement.clientWidth > 814) {
        scrollMenu();
        changeMenuActiveItem();
    } else {
        showMenu();
        showScrollMobile();
    }

});