export default (function () {
    var windowYval = document.getElementById('main-slider').offsetHeight - 190;
    var alreadyFixed = false;

    window.addEventListener("scroll", function () {

        var scrollVal = window.pageYOffset;

        if (scrollVal >= windowYval && !alreadyFixed) {
            document.getElementById('menu').classList.add('header__wrap--fixed');
            alreadyFixed = true;
        }
        if (scrollVal < windowYval && alreadyFixed) {
            document.getElementById('menu').classList.remove('header__wrap--fixed');
            alreadyFixed = false;
        }
    });

});

