export default (function() {
    var arrayOfIds =['main-slider','about', 'services','projects', 'team', 'news', 'clients', 'contact'];
    var flag = false;
    var menuHrefs = document.getElementsByClassName('menu__item-href');
    for (var i = 0; i < menuHrefs.length; i++) {
        let k = i;
        menuHrefs[i].addEventListener('click', function() {
            console.log(k);
            if ((document.getElementsByClassName('menu__item-href')[k].nextSibling == null)) {
                window.smoothScroll(document.getElementById(arrayOfIds[k]));
            }
            else if (flag) {
                document.getElementsByClassName('header__list')[0].classList.remove('header__list--visible');
                window.smoothScroll(document.getElementById(arrayOfIds[k]));
                flag = false;
            } else {
                flag = true;
            }

        })
    }

})

window.smoothScroll = function(target) {
    var scrollContainer = target;
    do { //find scroll container
        scrollContainer = scrollContainer.parentNode;
        if (!scrollContainer) return;
        scrollContainer.scrollTop += 1;
    } while (scrollContainer.scrollTop == 0);

    var targetY = 0;
    do { //find the top of target relatively to the container
        if (target == scrollContainer) break;
        targetY += target.offsetTop;
    } while (target = target.offsetParent);

    var scroll = function(c, a, b, i) {
        i++; if (i > 30) return;
        c.scrollTop = a + (b - a) / 30 * i;
        setTimeout(function(){ scroll(c, a, b, i); }, 1);
    }
    // start scrolling
    scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
}