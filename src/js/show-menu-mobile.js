export default (function() {
    var isVisible = false;
    var element = document.getElementsByClassName('header__list')[0];

    document.getElementsByClassName('header__menu-icon')[0].addEventListener('click', function(){
        event.preventDefault();

        if (!isVisible) {
            isVisible = true;
            element.classList.add('header__list--visible');

        } else {
            isVisible = false;
            element.classList.remove('header__list--visible');
        }

    })
})