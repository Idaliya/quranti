export default (function() {
    var arrayOfIds =['main-slider','about', 'services','projects', 'team', 'news', 'clients', 'contact'];
    var arrayOfOffsets = [];
    for (var i = 0; i < arrayOfIds.length; i++) {
        arrayOfOffsets[i] = getOffsetSum(document.getElementById(arrayOfIds[i])) - 200;
    }
    arrayOfOffsets[arrayOfOffsets.length-1] = arrayOfOffsets[arrayOfOffsets.length-1] - 100;
    arrayOfOffsets[arrayOfOffsets.length] = arrayOfOffsets[arrayOfOffsets.length-1] + 1000;

    var activeId = 0;

    window.addEventListener("scroll", function(){
        var scrollVal = window.pageYOffset;
        for (var j = 0; j < arrayOfOffsets.length; j++) {
            if ((scrollVal >  arrayOfOffsets[j]) && (scrollVal <  arrayOfOffsets[j+1])) {
                if (activeId == j) {
                    break;
                }
                document.getElementById('menu-list').getElementsByClassName('item-href')[activeId].classList.remove('item-href--active');
                document.getElementById('menu-list').getElementsByClassName('item-href')[j].classList.add('item-href--active');
                activeId = j;
                break;
            }
        }
    });
});


function getOffsetSum(elem) {
    var top=0;
    while(elem) {
        top = top + parseFloat(elem.offsetTop)
        elem = elem.offsetParent

    }
    return Math.round(top)
}