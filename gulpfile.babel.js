import path from 'path';
import fs from 'fs';
import gulp from 'gulp';
import del from 'del';
import browserify from 'browserify';
import watchify from 'watchify';
import babelify from 'babelify';
import {assign} from 'lodash';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import runSequence from 'run-sequence';

const $ = require('gulp-load-plugins')();
const dev = process.argv.indexOf('--production') === -1;
const browserSync = require('browser-sync').create();

gulp.task('clean', del.bind(null, ['dist']));

gulp.task('jade', () => {
    function datasource(file) {
    const block = path.basename(file, `.css.json`);
    return JSON.parse(fs.readFileSync(`src/blocks/${block}/${file}`, 'utf8'));
}

gulp.src('src/*.jade')
    .pipe($.jade({
        pretty: dev,
        locals: {
            datasource,
            require
        }
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('jade:watch', ['jade'], browserSync.reload());

gulp.task('css', () => (
    gulp.src('src/**/*.css')
        .pipe($.if(dev, $.sourcemaps.init()))
        .pipe($.postcss([
            require('postcss-use')({
                modules: ['postcss-assets']
            }),
            require('postcss-import')({
                glob: true
            }),
            require('postcss-assets')({
                basePath: 'src'
            }),
            require('postcss-at2x'),
            require('postcss-nested'),
            require('autoprefixer')({
                browsers: ['last 2 versions', 'ie >= 8']
            }),
            require('postcss-flexibility'),
            require('cssnano'),
            require('postcss-browser-reporter'),
            require('postcss-reporter')()
        ]))
        .pipe($.concat('main.css'))
        .pipe($.if(dev, $.sourcemaps.write('./')))
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.reload({stream: true}))
)
);

const customOpts = {
    entries: ['src/main.js'],
    debug: dev
};
const opts = assign({}, watchify.args, customOpts);
const b = dev ? watchify(browserify(opts)) : browserify(opts);
b.transform(babelify);

function bundle() {
    return b.bundle()
            .on('error', err => {
            $.util.log(err.message);
    browserSync.notify('Browserify Error!');
})
.pipe(source('main.js'))
    .pipe(buffer())
    .pipe($.sourcemaps.init({loadMaps: true}))
    .pipe($.if(!dev, $.uglify()))
    .pipe($.if(dev, $.sourcemaps.write('./')))
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream({once: true}));
}

gulp.task('js', bundle);
b.on('update', bundle);
b.on('log', $.util.log);

gulp.task('lint', () => (
    gulp.src(['src/**/*.js'])
        .pipe($.eslint())
        .pipe($.eslint.format())
        .pipe($.eslint.failOnError())
)
);

gulp.task('images', () => {
    return gulp.src(['src/**/*.png', 'src/**/*.jpg'])
        .pipe(gulp.dest('dist'));
});

gulp.task('copy', () => {
    return gulp.src(['src/fonts**/*','src/owl-carousel**/*'])
        .pipe(gulp.dest('dist'));
});



gulp.task('serve', ['build'], () => {
    browserSync.init({
        notify: true,
        port: 9000,
        server: {
            baseDir: ['dist'],
            routes: {
                '/node_modules': 'node_modules'
            }
        }

    });

// gulp.watch([
// 'dist/main.js',
// ]).on('change', reload);

gulp.watch('src/**/*.css', ['css']);
gulp.watch('src/**/*.jade', ['jade:watch']);
});

gulp.task('build', callback => {
    runSequence(
        ['clean'],
            ['css'],
            ['jade', 'js', 'images', 'copy'],
            callback
    );
});